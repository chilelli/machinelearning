# Original Fields
sPassengerId = 'passengerid'
sSurvival = 'survived'
sPClass = 'pclass'
sName = 'name'
sSex = 'sex'
sAge = 'age'
sSibSp = 'sibsp'
sParch = 'parch'
sTicket = 'ticket'
sFare = 'fare'
sCabin = 'cabin'
sEmbarked = 'embarked'

# Created Fields
sCategoryAge = 'cat_age'
sCategoryFare = 'cat_fare'
sRelatedPeople = 'family_on_board'
sTitles = 'title'


dictTitles = {
    "Capt": "o",
    "Col": "o",
    "Major": "o",
    "Jonkheer": "r",
    "Don": "r",
    "Dona": "r",
    "Sir": "r",
    "Dr": "o",
    "Rev": "o",
    "the Countess": "r",
    "Mme": "Mrs",
    "Mlle": "Miss",
    "Ms": "Mrs",
    "Mr": "Mr",
    "Mrs": "Mrs",
    "Miss": "Miss",
    "Master": "MS",
    "Lady": "r",
}