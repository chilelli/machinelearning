import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import missingno
import pandas as pd

class csPlotDists:

  def funcPlotDefaultRate(df, sFieldDist, sDefaultField):

    plt.figure(figsize=(20, 6), dpi=80)

    sns.countplot(data = df, x = sDefaultField, hue = sFieldDist)
    plt.title('Survivors for {} variable'.format(sFieldDist))
    plt.ylabel('Total Occurrences')
    plt.xlabel(sFieldDist)
    plt.ylim(ymin=0)
    plt.legend(loc='upper right', title = sFieldDist)
    plt.show()
    
  def funcBoxHistPlot(df, ssFields, ssFigSize):
    
    iFields = len(ssFields)
    fig, axarr = plt.subplots(2, iFields, figsize = ssFigSize)

    iPosCount = 0
    for sField in ssFields:
      sns.boxplot(x = df[sField], ax = axarr[0, iPosCount])
      axarr[0, iPosCount].set_title("BoxPlot for {} Variable".format(sField))

      df[[sField]].hist(ax = axarr[1, iPosCount])
      axarr[1, iPosCount].set_title("Histogram for {} Variable".format(sField))

      iPosCount += 1
  
  def funcCorrelationPlot(df, sStyle):
    corr = df.corr()

    # Generate a mask for the upper triangle
    mask = np.triu(np.ones_like(corr, dtype=bool))

    # Set up the matplotlib figure
    plt.figure(figsize = (15, 7))

    # Draw the heatmap with the mask and correct aspect ratio
    sns.heatmap(corr, mask=mask, cmap = sStyle, linewidths=1, annot = True)


class csMissingValuesAnalysis:

  # Plots a distribution 
  def funcPlotValues(dfTraining, dfTesting):
    fig, axarr = plt.subplots(1,2, figsize=(20,7))

    missingno.matrix(dfTraining,figsize=(10, 7), fontsize=9, ax = axarr[0])
    axarr[0].set_title("Missing Value Distribution in Train Dataset")
    missingno.matrix(dfTesting, figsize=(10, 7), fontsize=9, ax = axarr[1])
    axarr[1].set_title("Missing Value Distribution in Test Dataset")

    # Tight layout often produces nice results
    # but requires the title to be spaced accordingly
    fig.tight_layout()
    fig.subplots_adjust(top=0.90)

    plt.show()

  def funcShowMissDist(df):
    print(df.isnull().sum().sort_values(ascending=False)/len(df))

  def funcShowMissingNUnique(df, sField):
    print('------------------------------------------------')
    print('Unique {0} values: {1}'.format(sField, df[sField].nunique()))
    print(' ')
    print('Field Data Distribution')
    print(df[sField].value_counts(dropna = False))



class csMissingValues(csMissingValuesAnalysis):

  def funcMissingValueFilling(df, ssFields, ssMethods):
    # Fill missing data
    for sField, sMethod in zip(ssFields, ssMethods):
        if sMethod == 'mean':
          df[sField].fillna(df[sField].mean(), inplace=True)
        if sMethod == 'mode':
          df[sField].fillna(df[sField].mode()[0], inplace=True)
        if sMethod == 'drop':
          df[sField].dropna(axis = 1, inplace = True)
    return(df)